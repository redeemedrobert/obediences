import Menu from './components/Menu';
import Page from './pages/Page';
import Today from './pages/Today'
import About from './pages/about'
import Manage from './pages/Manage'
import React, { useState } from 'react';
import { IonApp, IonRouterOutlet, IonSplitPane } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { Redirect, Route } from 'react-router-dom';

import { LocalNotifications as LN } from '@ionic-native/local-notifications'

import * as Interfaces from './scripts/Interfaces'
import * as IO from './scripts/io'
import { v4 as uuidv4 } from 'uuid'

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

const App: React.FC = () => {
  const [dailyObds, setDailyObds] = useState<Interfaces.DailyObd[]>([])/*
    [{
    key: uuidv4(),
    title: 'Morning prayers',
    completed: true
  },{
    key: uuidv4(),
    title: 'Canon of Repentance',
    completed: false
  }])*/

  window.addEventListener('DOMContentLoaded', ()=>{
    IO.loadDailyObdsFromFile.then(
      res=>setDailyObds(res),
      er=>console.error(er)
    )
  })

  function setDailies(changedObd:Interfaces.DailyObd):void{
    const output:Interfaces.DailyObd[] = dailyObds.map(item=>{
      if(item.key === changedObd.key) return changedObd
      return item
    })
    setDailyObds(output)
    IO.saveDailyObds(output)
  }

  function addDaily(title:string, time:string):void{
    const key:string = uuidv4()
    const nid:number = new Date().getMilliseconds()
    const output:Interfaces.DailyObd[] = [...dailyObds, {
      key: key,
      title: title,
      completedDate: undefined,
      time: time,
      notificationID: nid
    }]
    const hour:number = parseInt(time.split(':')[0])
    const minute:number = parseInt(time.split(':')[1])
    const hourOut:number = hour<13 ? hour : hour-12
    const ampm:string = hour<12 ? 'am' : 'pm'
    LN.schedule({
      id: nid,
      title: 'Daily Obedience Due',
      text: `${title} at ${hourOut}:${minute}${ampm}`,
      trigger: { every: { hour: hour, minute: minute }}
    })
    setDailyObds(output)
    IO.saveDailyObds(output)
  }

  function delDaily(key:string):void{
    const output:Interfaces.DailyObd[] = dailyObds.filter((item)=>item.key!==key)
    LN.cancel(dailyObds.filter((item)=>item.key===key)[0].notificationID)
    setDailyObds(output)
    IO.saveDailyObds(output)
  }

  return (
    <IonApp>
      <IonReactRouter>
        <IonSplitPane contentId="main">
          <Menu />
          <IonRouterOutlet id="main">
            <Route path="/page/today" render={(props)=>(<Today {...props} todayProps={{items: dailyObds, setter: setDailies}} /> )} />
            <Route path="/page/manage" render={(props)=>(<Manage {...props} manageProps={{dailies: dailyObds, addDaily: addDaily, delDaily: delDaily}} />)} />
            <Route path="/page/about" render={()=>(<About />)} />
            <Route path="/page/:name" component={Page} />
            <Redirect from="/" to="/page/today" exact />
          </IonRouterOutlet>
        </IonSplitPane>
      </IonReactRouter>
    </IonApp>
  )
}

export default App