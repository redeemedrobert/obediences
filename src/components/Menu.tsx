import {
  IonContent,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonMenu,
  IonMenuToggle,
  IonNote,
} from '@ionic/react';

import React from 'react';
import { useLocation } from 'react-router-dom';
import { todayOutline, fileTrayStackedOutline, documentTextOutline, newspaperOutline, settingsOutline } from 'ionicons/icons';
import './Menu.css';

interface AppPage {
  url: string;
  iosIcon: string;
  mdIcon: string;
  title: string;
}

const appPages: AppPage[] = [
  {
    title: 'Today\'s Obediences',
    url: '/page/today',
    iosIcon: todayOutline,
    mdIcon: todayOutline
  },
  {
    title: 'Manage',
    url: '/page/manage',
    iosIcon: fileTrayStackedOutline,
    mdIcon: fileTrayStackedOutline
  },
  {
    title: 'Reports',
    url: '/page/reports',
    iosIcon: newspaperOutline,
    mdIcon: newspaperOutline
  },
  {
    title: 'Settings',
    url: '/page/settings',
    iosIcon: settingsOutline,
    mdIcon: settingsOutline
  },
  {
    title: 'About',
    url: '/page/about',
    iosIcon: documentTextOutline,
    mdIcon: documentTextOutline
  }
];

const Menu: React.FC = () => {
  const location = useLocation();

  return (
    <IonMenu contentId="main" type="push">
      <IonContent>
        <IonList id="inbox-list">
          <IonListHeader>Obediences</IonListHeader>
          <IonNote>Where spiritual obedience flourishes.</IonNote>
          {appPages.map((appPage, index) => {
            return (
              <IonMenuToggle key={index} autoHide={false}>
                <IonItem className={location.pathname === appPage.url ? 'selected' : ''} routerLink={appPage.url} routerDirection="none" lines="none" detail={false}>
                  <IonIcon slot="start" icon={appPage.iosIcon} />
                  <IonLabel>{appPage.title}</IonLabel>
                </IonItem>
              </IonMenuToggle>
            );
          })}
        </IonList>
      </IonContent>
    </IonMenu>
  );
};

export default Menu;
