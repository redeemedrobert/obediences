import { IonButtons, IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar, IonList, IonItem, IonInput, IonIcon, IonButton, IonDatetime } from '@ionic/react';
import { closeOutline } from 'ionicons/icons'
import React, { useState } from 'react';
import { RouteComponentProps } from 'react-router'
//import Datey from '../scripts/Datey'
//import { v4 as uuidv4 } from 'uuid'
import * as Interfaces from '../scripts/Interfaces'

//const datey = new Datey(new Date())
let hour:number = new Date().getHours()
const startMin:number = new Date().getMinutes()
let minute:number = 0
if(startMin<15) minute = 0
if(startMin>15 && startMin < 45) minute = 30
if(startMin>44){ minute=0; hour=hour<23?hour+1:0}
const hmString:string = (hour<10?"0"+String(hour):String(hour)) + ":" + (minute<10?"0"+String(minute):String(minute))

const Manage:React.FC<Manage> = (manageProps)=>{
  let [ dailyInputValue, dailyInputSetter ] = useState<string>("")
  let dailyTimeString:string = hmString
  const addDailyInput = <IonInput id="addDailyInput" onIonChange={(e)=>dailyInputSetter(e.detail.value!)} enterkeyhint="done" value={dailyInputValue} placeholder="New Daily Obedience (type a name here, pick a time below and press enter/submit to submit)" />
  const dailyObdsSorted:Interfaces.DailyObd[] = manageProps.manageProps.dailies.sort((a,b)=>{
    const aNum:number = parseInt(a.time.replace(":",""))
    const bNum:number = parseInt(b.time.replace(":",""))
    return aNum-bNum
  })
  const tftt:(input:string)=>string = (input:string)=>{//24 to 12
    const split:Array<string> = input.split(":")
    let hours:number = parseInt(split[0])
    const ampm:string = hours<13?"am":"pm"
    hours = hours<13?hours:hours-12
    return String(hours)+":"+split[1]+ampm
  }

  return(
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Manage Obediences</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Schedule</IonTitle>
          </IonToolbar>
        </IonHeader>
        <h6>Daily obediences</h6>
        <IonList>
          {
            dailyObdsSorted.map(
              obd=>(
                <IonItem key={obd.key}>{obd.title} - {tftt(obd.time)}
                  <IonButton color="danger" slot="end" shape="round" onClick={()=>{if(window.confirm(`Are you sure you want to delete "${obd.title}"?`)){manageProps.manageProps.delDaily(obd.key)}}}>
                    <IonIcon icon={closeOutline} />
                  </IonButton>
                </IonItem>
              )
            )
          }
          <form onSubmit={(e)=>{
            e.preventDefault()
            if(dailyInputValue.length<1) return alert('Please choose an obedience name.')
            manageProps.manageProps.addDaily(dailyInputValue, dailyTimeString)
            dailyInputSetter('')
          }}>{addDailyInput}
          <IonDatetime mode="ios" displayFormat="h:mm a" minuteValues="0,15,30,45" value={hmString} onIonChange={e=>dailyTimeString=e.detail.value!} />
          <IonButton expand="block" type="submit" mode="ios" disabled={dailyInputValue!=="New Obedience"&&dailyInputValue!==""?false:true}>Submit</IonButton>
          </form>
        </IonList>
      </IonContent>
    </IonPage>
  )
}

interface Manage extends RouteComponentProps{
  manageProps: {
    dailies: Interfaces.DailyObd[]
    addDaily: (title: string, time:string)=>void
    delDaily: (key:string)=>void
  }
}

export default Manage
