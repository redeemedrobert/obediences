import React from 'react'
import { RouteComponentProps } from 'react-router'
import { IonPage, IonHeader, IonButtons, IonMenuButton, IonTitle, IonToolbar, IonContent, IonList, IonItem, IonCheckbox, IonText } from '@ionic/react'
import * as Interfaces from '../scripts/Interfaces'
import Datey from '../scripts/Datey'

const datey = new Datey(new Date())
const fullDate = new Date()
const year:number = fullDate.getFullYear()
const month:number = fullDate.getMonth()
const date:number = fullDate.getDate()
const todayInt:string = String(year) + (month>9?"":"0") + String(month) + (date>9?"":"0") + date

const Today: React.FC<Today> = (todayProps)=>{
  const dailyObdsSorted:Interfaces.DailyObd[] = todayProps.todayProps.items.sort((a,b)=>{
    const aNum:number = parseInt(a.time.replace(":",""))
    const bNum:number = parseInt(b.time.replace(":",""))
    return aNum-bNum
  })
  const tftt:(input:string)=>string = (input:string)=>{//24 to 12
    const split:Array<string> = input.split(":")
    let hours:number = parseInt(split[0])
    const ampm:string = hours<13?"am":"pm"
    hours = hours<13?hours:hours-12
    return String(hours)+":"+split[1]+ampm
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start"><IonMenuButton /></IonButtons>
          <IonTitle>Today's Obediences</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <h1 style={titleStyle}>{datey.getDateString()}</h1>
        <IonList>
          <h6>Daily obediences</h6>
          {
            dailyObdsSorted.map(
              obd=>(
                <IonItem key={obd.key}>
                  <IonCheckbox onIonChange={()=>todayProps.todayProps.setter({
                    key: obd.key,
                    title: obd.title,
                    completedDate: obd.completedDate===todayInt?undefined:todayInt,
                    time:obd.time,
                    notificationID: obd.notificationID
                  })}
                  checked={obd.completedDate===todayInt ? true : false} /> <IonText>&nbsp;{obd.title}</IonText>
                  <IonText slot="end" >Due by {tftt(obd.time)}</IonText>
                </IonItem>
              )
            )
          }
          <h6>Scheduled just for today</h6>
          <IonItem>Nothing to show.</IonItem>
        </IonList>
      </IonContent>
    </IonPage>
  )
}

interface Today extends RouteComponentProps {
  todayProps: {items: Interfaces.DailyObd[], setter: (obd:Interfaces.DailyObd)=>void}
}

const titleStyle = {
  textAlign: 'center' as 'center'
}
export default Today