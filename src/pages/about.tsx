import { IonButtons, IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import React from 'react';

const About:React.FC = ()=>{
  return(
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>About Obediences App</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">About Obediences</IonTitle>
          </IonToolbar>
        </IonHeader>
        <div className='container' style={containerStyle}>Grown with &hearts; using TypeScript, ReactJS, Ionic Framework, CapacitorJS, and other open source softwares.<br />The source code for this app is open and can be found at redeemedrobert.com/obd</div>
      </IonContent>
    </IonPage>
  )
}

const containerStyle = {
  textAlign: 'center' as 'center',
  position: 'absolute' as 'absolute',
  fontSize: '15px',
  top: '50%'
}

export default About
