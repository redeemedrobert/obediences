//Datey is a script used to assist in the construction of calendars and other date related web components.
import { v4 as uuidv4 } from 'uuid'
export default class Datey{
  date: Date
  monthDays: Array<number>
  dayNames: Array<string>
  monthNames: Array<string>

  constructor(date: Date){
    this.date = date
    const year: number = date.getFullYear()
    this.monthDays = [
      31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ]
    //check for leap year
    if(year % 4 === 0){ //leap year is always evenly divisible by 4
      this.monthDays[1] = 29
      if(year % 100 === 0){ //if evenly divisible by 100, it't not a leap year
        this.monthDays[1] = 28
        if(year % 400 === 0){ //unless also evenly divisible by 400!
          this.monthDays[1] = 29
        }
      }
    }
    this.dayNames = [
      'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
    ]
    this.monthNames = [
      'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
    ]
  }

  getWeek(refDate?:Date){
    let week:Array<any> = []
    const thisDay: number = refDate ? refDate.getDay() : this.date.getDay()
    const thisDate: number = refDate ? refDate.getDate() : this.date.getDate()
    const thisMonth: number = refDate ? refDate.getMonth() : this.date.getMonth()
    //if(thisDay !== 0){
      for(let i=0; i<7; ++i){
        let outYear: number = refDate ? refDate.getFullYear() : this.date.getFullYear()
        let outMonth: number = thisMonth
        let outDate: number = thisDate + (i-thisDay)
        if(outDate < 1){
          outMonth = thisMonth-1 > -1 ? thisMonth-1 : 11
          outDate = this.monthDays[outMonth] - (0-outDate)
          if(outMonth === 11) outYear -= 1
        }
        if(outDate > this.monthDays[thisMonth] && outMonth === thisMonth){
          outMonth = thisMonth+1 < 12 ? thisMonth+1 : 0
          outDate = outDate - this.monthDays[thisMonth]
          if(outMonth === 0) outYear += 1
        }
        week.push({
          key: uuidv4(),
          day: i,
          dayName: this.dayNames[i],
          date: outDate,
          month: outMonth,
          monthName: this.monthNames[outMonth],
          year: outYear
        })
      }
    //}
    return week
  }

  getMonth(){
    let month = []
    const firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1)
    let weeksInMonth: number = 4
    const daysOnCalMonth: number = firstDay.getDay() + this.monthDays[this.date.getMonth()]
    if(daysOnCalMonth > 28) weeksInMonth = 5
    if(daysOnCalMonth > 35) weeksInMonth = 6
    for(let i=0; i<weeksInMonth; ++i){
      let fetchYear = this.date.getFullYear()
      let fetchMonth = this.date.getMonth()
      let fetchDate = i===0 ? 1 : (i*7)+1
      if(fetchDate > this.monthDays[fetchMonth]){
        fetchDate = fetchDate - this.monthDays[fetchMonth]
        fetchMonth = fetchMonth < 11 ? fetchMonth+1 : 0
        fetchYear = fetchMonth === 0 ? fetchYear + 1 : fetchYear
      }
      const refDate = new Date(fetchYear, fetchMonth,  fetchDate)
      month.push(this.getWeek(refDate))
    }
    return month
  }

  getDateString():string{
    let output:string = ''
    const dateNum = this.date.getDate()
    const dayNames:Array<string> = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
    const monthNames:Array<string> = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    const dateLetters = (date:number)=>{
      if(dateNum === 1 || dateNum-20 === 1 || dateNum-30 === 1) return 'st'
      if(dateNum === 2 || dateNum-20 === 2) return 'nd'
      if(dateNum === 3 || dateNum-20 === 3) return 'rd'
      return 'th'
    }
    output = dayNames[this.date.getDay()] + ', '
    output += monthNames[this.date.getMonth()] + ' '
    output += String(dateNum) + dateLetters(dateNum) + ', '
    output += String(this.date.getFullYear())
    return output
  }
}
