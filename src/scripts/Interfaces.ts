
export interface DailyObd{
    key: string
    title: string
    completedDate: string|undefined
    time: string
    notificationID: number
}