import { Plugins, FilesystemDirectory } from '@capacitor/core'
import { Toast } from '@ionic-native/toast'
import * as Interfaces from './Interfaces'
const { Filesystem } = Plugins

export const loadDailyObdsFromFile = new Promise((resolution:(value?:Interfaces.DailyObd[])=>void, reject)=>{
    Filesystem.readFile({
        directory: FilesystemDirectory.Data,
        path: 'dailyObds.json'
    }).then(
        (res)=>{
            try {
                const readObj:Interfaces.DailyObd[] = JSON.parse(res.data.replace(new RegExp('COMMA', 'g'), ','))
                //The file has commas removed and replaced by the word COMMA, as the Capacitor filesystem plugin
                //doesn't seem to be processing commas correctly in Capacitor version 2.1.0.
                if(readObj){
                    Toast.show('Loaded saved obediences from file.', '2000', 'bottom')
                    console.log('Loaded daily obediences from dailyObds.json.')
                    resolution(readObj)
                }else{
                    console.log('Nothing to load from dailyObds.json.')
                    reject('Nothing found in dailyObds.json, or the file format is corrupted.')
                }
            } catch(er) { reject(er) }
        },
        (er)=>{
            Toast.show('Unable to load saved obediences from file.', '2000', 'bottom')
            console.error('Did not load dailyObds.json: ', er)
            reject(er)
        }
    )
})

export function saveDailyObds(toSave:Interfaces.DailyObd[]):void{
    Filesystem.writeFile({
        data: JSON.stringify(toSave).replace(new RegExp(',','g'), "COMMA"),
        //The file has commas removed and replaced by the word COMMA, as the Capacitor filesystem plugin
        //doesn't seem to be processing commas correctly in Capacitor version 2.1.0.
        path: 'dailyObds.json',
        directory: FilesystemDirectory.Data
      }).then((res)=>{
        Toast.show('Saved successfully.', '2000', 'bottom')
        console.log('Daily obediences saved to: ', res.uri)
      }, (er)=>{
        Toast.show('Save failed. Please try again.', '2000', 'bottom')
        console.error(er)
      })
}